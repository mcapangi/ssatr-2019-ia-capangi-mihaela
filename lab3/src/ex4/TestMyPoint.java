
package ex4;

import sun.applet.Main;

public class TestMyPoint {
    
    public static void main(String []args){
        MyPoint a = new MyPoint();
        MyPoint b = new MyPoint();
        a.setX(5);
        b.setY(5);
        System.out.println(a.toString());
        System.out.println(b.toString());
        System.out.println("distanta a-b: "+a.distance(b));
        System.out.println("distanta a-(0,5): "+a.distance(0,5));
    }
            
}

//Exercise 4
//A class called MyPoint, which models a 2D point with x and y coordinates contains:
//
//Two instance variables x (int) and y (int).
//A “no-argument” (or “no-arg”) constructor that construct a point at (0, 0).
//A constructor that constructs a point with the given x and y coordinates.
//Getter and setter for the instance variables x and y.
//A method setXY() to set both x and y.
//A toString() method that returns a string description of the instance in the format “(x, y)”.
//A method called distance(int x, int y) that returns the distance from this point to another point at the given (x, y) coordinates.
//An overloaded distance(MyPoint another) that returns the distance from this point to the given MyPoint instance another.
//Write the code for the class MyPoint. Also write a test class (called TestMyPoint) to test all the methods defined in the class.
package ex4;

public class MyPoint {

    private int x, y;

    public MyPoint() {
        x = 0;
        y = 0;
    }

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }
    
    public void setX(int x){
        this.x=x;
    }
    
    public int getY() {
        return y;
    }
    
    public void setY(int y){
        this.y=y;
    }
    
    public String toString(){
        return String.format("(%d, %d)", x,y);
    }
    
    public double distance(int x, int y){
        return Math.sqrt(Math.pow(this.x-x,2)+Math.pow(this.y-y,2));
    }
    
    public double distance(MyPoint p){
        return Math.sqrt(Math.pow(p.x-x,2)+Math.pow(p.y-y,2));
    }
            
}

//A method called distance(int x, int y) that returns the distance from this point to another point at the given (x, y) coordinates.
//An overloaded distance(MyPoint another) that returns the distance from this point to the given MyPoint instance another.
//Write the code for the class MyPoint. Also write a test class (called TestMyPoint) to test all the methods defined in the class.
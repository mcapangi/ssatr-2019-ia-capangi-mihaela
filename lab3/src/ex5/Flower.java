/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex5;

public class Flower {

    int petal;
    static int numberOfFlowers = 0;

    Flower() {
        petal=0;
        System.out.println("A flower with 0 petals has been created!");
        numberOfFlowers++;
    }

    Flower(int petal) {
        this.petal=petal;
        System.out.println("A flower with "+petal+" petals has been created!");
        numberOfFlowers++;
    }
    
    public int getPetal(){
        return petal;
    }

    public void setPetal(int petal) {
        this.petal = petal;
    }
     
    public static int getNumberOfFlowers() {
        return numberOfFlowers;
    }

    public void finalize()
    {
        System.err.println("Delete flower with "+petal+" petals.");
    }
}

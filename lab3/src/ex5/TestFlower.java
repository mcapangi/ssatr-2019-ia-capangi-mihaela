/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex5;

/**
 *
 * @author Profesor
 */
public class TestFlower {

    public static void main(String[] args) {
        Flower[] garden = new Flower[5];
        for (int i = 0; i < 5; i++) {
            garden[i] = new Flower(i);
        }
        Flower f = new Flower();

        System.out.println("There are " + Flower.numberOfFlowers + " flowers in the garden.");

        Flower myF = null;
        int i = 0;
        while (true) {
            i++;
            myF = new Flower(i);
        }
    }
}

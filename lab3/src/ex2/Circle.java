//A class called Circle contains:
//
//Two private instance variables: radius (of type double) and color (of type String), with default value of 1.0 and “red”, respectively.
//Two overloaded constructors;
//Two public methods: getRadius() and getArea().
//Write a class which models the class Circle. Write a class TestCircle which test Circle class.
package ex2;


public class Circle {

    private Double radius;
    private String color;
    
    public Circle()
    {
        radius=1.0;
        color="red";
    }
    
    public Circle(double radius)
    {
        this.radius=radius;
        this.color="red";
    }

    public Circle(String color) {
        this.color = color;
        this.radius=1.0;
    }
    
    double getRadius()
    {
        return radius;
    }
    
    double getArea()
    {
        return 2*Math.PI*this.radius;
    }
    
}

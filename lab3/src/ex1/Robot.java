//A class Robot contains:
//
//One instance variable 'x' (of type int) representing the position of robot;
//One default constructor which initialize the value to 1;
//One change(int k) method which add to the current robot x value to k (as long as k >= 1);
//One toString() method which returns the position of robot;
//Write a class which models the class Robot . Write a class TestRobot which test Robot class.
package ex1;

/**
 *
 * @author Profesor
 */
public class Robot {
    private int x;
    public Robot()
    {
        x=1;
    }
    public void change(int k)
    {
        if(k>=1)
            x+=k;
    }
    public String toString()
    {
        return x+"";
    }
}

/*
 * Exercise 6
    Being given an int number N, compute N! using 2 methods:
        a non recursive method
        a recursive method
 */
package lab2;

import java.util.Scanner;


public class Ex6 {
    int factorialR(int n)
    {
        if(n==0)
            return 1;
        else
            return n*factorialR(n-1);
    }
    
    int factorial(int n)
    {
        int f=1;
        while(n>0)
             f*=n--;
        return f;
    }
    
    void rezolva()
    {
        Scanner nr=new Scanner(System.in);
        System.out.println("Introduceti un nr: ");
        int n=nr.nextInt();
        String mesaj=String.format("Recursiv %d!=%d",n, factorialR(n));
        System.out.println(mesaj);
        mesaj=String.format("Iterativ %d!=%d",n, factorial(n));
        System.out.println(mesaj);
    }
}

/*
Exercise 1
    Write a program which reads 2 numbers from keyboard and display the maximum between them.
*/
package lab2;
import java.awt.BorderLayout;
import java.util.Scanner;

public class Ex1 {

    int maxim(int x, int y)
    {
        return (x>y?x:y);
    }
    void rezolva( ) {
        Scanner numar=new Scanner(System.in);
        System.out.println("Introduceti doua numere: ");
        int x=numar.nextInt();
        int y=numar.nextInt();
        System.out.println("Maximul: "+maxim(x,y));
    }
    
}


package lab2;

import java.util.Scanner;

public class Lab2 {
     public static void main(String[] args) {
        Ex1 ex1=new Ex1();
        Ex2 ex2=new Ex2();
        Ex3 ex3=new Ex3();
        Ex4 ex4=new Ex4();
        Ex5 ex5=new Ex5();
        Ex6 ex6=new Ex6();
        Ex7 ex7=new Ex7();
       
        Scanner nr=new Scanner(System.in);
        int n;
        do{
            System.out.println("Numarul exercitiului [1...7], alt nr => Stop: ");
            n=nr.nextInt();
            switch(n)
            {
                case 1:
                    ex1.rezolva();
                    break;
                case 2:
                    ex2.rezolva();
                    break;
                case 3:
                    ex3.rezolva();
                    break;
                case 4:
                    ex4.rezolva();
                    break;
                case 5:
                    ex5.rezolva();
                    break;
                case 6:
                    ex6.rezolva();
                    break;
                case 7:
                    ex7.rezolva();
                    break;
                default:
                    System.out.println("Stop!");
            }
        }while(n>=1 && n<=7);
        
    }
}

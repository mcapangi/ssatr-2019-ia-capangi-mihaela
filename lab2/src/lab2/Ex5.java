/*
Exercise 5
    Write a program which generate a vector of 10 int elements, sort them using bubble sort method and 
    then display the result.
*/
package lab2;
import java.util.Random; 

public class Ex5 {
    int[] generate(int n){
        int []s=new int[n];
        Random r=new Random();
        for(int i=0; i<n; i++)
            s[i]=r.nextInt() % 100;
        return s;
    }
    int[] sort(int[]s){
        int i, aux;
        boolean ok;
        do{
            ok=true;
            for(i=0; i<s.length-1; i++)
            {
                if(s[i]>s[i+1])
                {
                    aux=s[i];
                    s[i]=s[i+1];
                    s[i+1]=aux;
                    ok=false;
                }
            }
        }while (!ok);
        return s;
    }
    
    void write(int[] s)
    {
        for(int i=0; i<s.length; i++)
            System.out.print(s[i]+" ");  
    }
       
    void rezolva()
    {
        write(sort(generate(10)));
    }
}

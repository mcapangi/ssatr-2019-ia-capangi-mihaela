/*
Exercise 3
    Write a program which display prime numbers between A and B, where A and B are read from console. 
    Display also how many prime numbers have been found.
*/
package lab2;

import java.util.Scanner;


public class Ex3 {
    boolean prim(int n){
        if(n<2)
            return false;
        if(n==2)
            return true;
        if(n%2==0)
            return false;
        for(int d=3;d*d<=n;d+=2)
            if(n%d==0)
                return false;
        return true;
    }
    void rezolva(){
        Scanner nr=new Scanner(System.in);
        System.out.println("Intervalul [A, B]: ");
        int A=nr.nextInt();
        int B=nr.nextInt();
        
        int cate=0,i;
        if(A%2==0)
        {
            if(A==2){
                System.out.print(A+" ");
                cate++;
            }
            A++;  
        }
        for(i=A;i<=B;i+=2){
            if(prim(i)){
                System.out.print(i+" ");
                cate++;
            }
        }
        System.out.println("\nNumere prime afisate: "+ cate);
    }
}

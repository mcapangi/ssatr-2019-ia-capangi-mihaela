/*
Exercise 4
    Giving a vector of N elements, display the maximum element in the vector.
*/
package lab2;

import java.util.Scanner;


public class Ex4 {
    int max(int[]s){
        int m=s[0];
        for(int i=1;i<s.length;i++){
            m=(s[i]>m?s[i]:m);
        }
        return m;
    }
    int [] read(){
        int s[];
        Scanner nr=new Scanner(System.in);
        System.out.println("Cate numere? ");
        s=new int[nr.nextInt()];
        System.out.println("Numerele: ");
        for(int i=0;i<s.length;i++){
            s[i]=nr.nextInt();
        }   
        return s;
    }
    void rezolva(){
        System.out.println("Maximul din sir: "+max(read()));
    }
            
    
}

/*
 * Exercise 7
    Write a “Gues the number” game in Java. Program will generate a random number and will ask user 
    to guess it. If user guess the number program will stop. If user do not guess it program will display: 
    Wrong answer, your number it too high' or 'Wrong answer, your number is too low'. 
    Program will allow user maximum 3 retries after which will stop with message 'You lost'.
 */
package lab2;

import java.util.Random;
import java.util.Scanner;


public class Ex7 {
    void rezolva()
    {
        Random r=new Random();
        int theNumber= Math.abs(r.nextInt())%100;
        int tries=3,x;
        Scanner nr= new Scanner(System.in);
        System.out.println("Guess a number beteewn 0 and 99 [magic: 1000]");
        do
        {
            System.out.println("The number is... ");
            x=nr.nextInt();
            if(x==1000)
            {
                System.out.println("The number is "+theNumber);
                //return;
            }
            if(x==theNumber)
            {
                System.out.println("You win!");
                return;
            }
            else if(x>theNumber)
            {
                System.out.println("Wrong answer, your number it too high.");
            }
            else
            {
                System.out.println("Wrong answer, your number it too low.");
            }
            tries--;
            
        }while(tries>0);
        System.out.println("You lost. The number: "+theNumber);
    }
}

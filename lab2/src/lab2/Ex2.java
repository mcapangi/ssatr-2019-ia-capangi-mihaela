/*
Exercise 2
    Exercise PrintNumberInWord (nested-if, switch-case): Write a program called PrintNumberInWord 
    which prints “ONE”, “TWO”,… , “NINE”, “OTHER” if the int variable “number” is 1, 2,… , 9, 
    or other, respectively. Use (a) a “nested-if” statement; (b) a “switch-case” statement.
*/
package lab2;

import java.util.Scanner;

 
public class Ex2 {
    void PrintNumberInWordIf(int n){
       if(n==0)
            System.out.println("Zero");
       else if(n==1)
            System.out.println("One");
       else if(n==2)
            System.out.println("Two");
       else if(n==3)
            System.out.println("Three");
       else if(n==4)
            System.out.println("Four");
       else if(n==5)
            System.out.println("Five");
       else if(n==6)
            System.out.println("Six");
       else if(n==7)
            System.out.println("Seven");
       else if(n==8)
            System.out.println("Eight");
       else if(n==9)
            System.out.println("Nine");
       else 
            System.out.println("Other");
    }
    
    void PrintNumberInWordSwitch(int n){
       switch (n){
           case 0:
           System.out.println("Zero");break;
           case 1:
            System.out.println("One");break;
           case 2:
            System.out.println("Two");break;
           case 3:
            System.out.println("Three");break;
           case 4:
            System.out.println("Four");break;
           case 5:
            System.out.println("Five");break;
           case 6:
            System.out.println("Six");break;
           case 7:
            System.out.println("Seven");break;
           case 8:
            System.out.println("Eight");break;
           case 9:
            System.out.println("Nine");break;
           default: 
            System.out.println("Other");
       }    
    }
            
    void rezolva() {
        Scanner numar=new Scanner(System.in);
        System.out.println("if=1, switch=2");
        int opt=numar.nextInt();
        System.out.println("Un numar: ");
        int n=numar.nextInt();
        switch (opt){
            case 1:
                PrintNumberInWordIf(n);
                break;
            case 2:
                PrintNumberInWordSwitch(n);
                break;
            default:
                System.out.println("Optiune inexistenta!");  
        }
        
    }
}

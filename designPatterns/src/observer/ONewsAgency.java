/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer;

import java.util.Observable;

public class ONewsAgency extends Observable {
    private String news;
 
    public void setNews(String news) {
        this.news = news;
        setChanged();
        notifyObservers(news);
    }
}

package robots;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;
import java.util.Queue;
import java.util.Random;

public class Board extends Observable{
    
    private int size=10;
    private Point [][]points=new Point[size][size];
    
    private ArrayList<Observer> observers = new ArrayList<>();

    public void addObserver(Observer o) {
        observers.add(o);
    }

    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    private void execute(Point p) {
        observers.forEach((observer) -> {
            //if (observer instanceof EnergyObserver)
                observer.update(this,p);
        });
    }
    
   
    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
    
    public Board(int size)
    {
        this.size=size;
        for(int i=0;i<size;i++)
        {
            for(int j=0;j<size;j++)
            {
                points[i][j]=new Point(i,j);
            }
        }
    }
    Point pointAt(int x, int y)
    {
        if(x>=0 && x<size && y>=0 && y<size)
        { 
            return points[x][y];
        }
        else
            return null;
    }
    
    Point pointAt(Point p)
    {
        if(p.getX()>=0 && p.getX()<size && p.getY()>=0 && p.getY()<size)
            return points[p.getX()][p.getY()];
        else
            return null;
    }
    
    Boolean contains(Point p)
    {
        return (p.getX()>=0 && p.getX()<size && p.getY()>=0 && p.getY()<size );
    }
    
      
    public Point generatePoint()
    {
        int x=0, y=0;
        Random r=new Random();
        x=r.nextInt(size);
        y=r.nextInt(size);
        return pointAt(x,y);
    }
    
    public Point generateDanger()
    {
        Point p;//=pointAt(9,9);
       do {
            p=generatePoint();
        } while (p.getColor()!=Colors.EMPTY || p.getOwner()!=null);
        p.setEnergy(-1);
        p.setColor(Colors.DANGER);
        execute(p);
        return p;
    }
    
    public Point generateEnergy()
    {
        Random r=new Random();
        Point p;
        do {
            p=generatePoint();
        } while (p.getColor()!=Colors.EMPTY || p.isDanger());
        
        p.setEnergy(r.nextInt(9)+1);
        p.setColor(Colors.ENERGY);
        
        execute(p);
        
        return p;
    }
    
    public void write()
    {
        for(int i=0;i<size;i++)
        {
            for(int j=0;j<size;j++)
            {
                if(pointAt(i, j).getOwner()!=null)
                    pointAt(i, j).getOwner().write();
                else
                    points[i][j].write();
            }
            System.out.println("");System.out.println("");
        }
        System.out.println("");
    }
    private int[][] lee(Point source,Point dest)
    {
        int[][] a = new int[size][size];
        Point crt , next ;
        String dir = "NESV";
        Queue<Point> q = new LinkedList<>();
        q.add(source);
        a[source.getX()][source.getY()] = 1;
        while (!q.isEmpty()) {
            crt = q.poll();
            for (int k = 0; k < 4; k++) {
                next = crt.nextPoint(dir.substring(k, k+1));
                if(!contains(next))
                    continue;
                
                if (a[next.getX()][next.getY()] == 0 && !pointAt(next).isDanger()  )
                        //&& (pointAt(next).getOwner() == null || next.equals(dest))) 
                {
                    a[next.getX()][next.getY()] = a[crt.getX()][crt.getY()] + 1;
                    q.add(next);
                    if (next.equals(dest)) {
                        return a;
                    }
                }
            }
        }
        return a;
    }
    
    public Path findPath(Point source, Point dest)//
    {
        int[][] a = lee(source,dest);
        Point crt = dest, next=dest, rez=dest;
        String dir = "NESV";
        Path path=new Path();
        if (a[dest.getX()][dest.getY()] == 0) {
            return null;
        }
        
        while (!source.equals(next)) {
            path.push(crt);
            for (int k = 0; k < 4; k++) {
                next = crt.nextPoint(dir.substring(k, k+1));
                if(!contains(next))
                    continue;
                if (a[next.getX()][next.getY()] == a[crt.getX()][crt.getY()] - 1) {
                    crt = next;
                    break;
                }
            }
        }
        return path;
    }
    
    public int distance(Point source, Point dest)
    {
        int[][] a = lee(source, dest);
        if (a[dest.getX()][dest.getY()] == 0) {
            return -1;
        }
        else
        {
            return a[dest.getX()][dest.getY()]-1;
        }
        
    }
    public void writeLee(Point source, Point dest)
    {
        int[][] a = lee(source, dest);
        
            for(int i=0;i<6;i++)
            {
                for(int j=0;j<6;j++)
                    System.out.print(a[i][j]+" ");
                System.out.println("");
            }
        
        
    }
    public Boolean existsPath(Point source, Point dest)
    {
        int[][] a = lee(source, dest);
        return (a[dest.getX()][dest.getY()] != 0);        
    }
    
    
}
/*
public void run()
    {
        int sec=0,i=0,nre=0;
        Random r=new Random();
        while(true && i <10)
        {
            if(sec==0)
            {System.out.println(i+"");i++;
            write();
                generateEnergy();
                sec=r.nextInt(size)+1;
                System.out.println("Next on "+sec+"secs");
            }
            else
                sec--;
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
            
        }
    }
*/
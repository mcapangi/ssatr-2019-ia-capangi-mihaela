/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robots;

/**
 *
 * @author Profesor
 */
public class Point {

    private int x, y, energy;
    private String color;//, owner;
    private Robot owner;
    
    
    public Point() {
        this.color = Colors.EMPTY;
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
        this.color=Colors.EMPTY;
        this.energy=0;
    }
    
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
        if(energy==0)
            color=Colors.EMPTY;
        if(energy>0)
            color=Colors.ENERGY;
        if(energy<0)
            color=Colors.DANGER;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Robot getOwner() {
        return owner;
    }

    public void setOwner(Robot owner) {
        this.owner = owner;
    }

    public Point nextPoint(String dir) {
        Point tmp = new Point(x,y);
        switch(dir.toLowerCase())
        {
            case "n":;
            case "nord":tmp.x--;break;
            case "s":;
            case "sud":tmp.x++;break;
            case "v":;
            case "vest":tmp.y--;break;
            case "e":;
            case "est":tmp.y++;break;     
        }
        return tmp;
    }
    public Boolean isDanger()
    {
        return energy<0;
    }
    public Boolean isEnergy()
    {
        return energy>0;
    }
    public void write() {
        
        String mesaj=" ";
        if (this.energy > 0) 
            mesaj=energy+"";
        write(mesaj);
    }

    public void write(String mesaj) {
        System.out.print(color+ " " + mesaj + " " + Colors.RESET + " ");
    }
    
    
    public Boolean equals(Point p)
    {
        return x==p.x && y==p.y;
    }
    
    public void free()
    {
        if(this.owner!=null)this.owner=null;
        /*if(this.energy==0)
            this.color=Colors.EMPTY;
        else
            this.color=Colors.ENERGY;*/
    }
    
    public String toString()
    {
        return x+" "+y;
    }
}

//https://stackoverflow.com/questions/5762491/how-to-print-color-in-console-using-system-out-println

//System.out.println(ANSI_GREEN_BACKGROUND + "This text has a green background but default text!" + ANSI_RESET);
// System.out.println(ANSI_RED + "This text has red text but a default background!" + ANSI_RESET);
// System.out.println(ANSI_GREEN_BACKGROUND + ANSI_RED + "This text has a green background and red text!" + ANSI_RESET);
package robots;

import java.util.HashMap;

/**
 *
 * @author Profesor
 */
public class Colors {

    private static HashMap<String, String> list = new HashMap<String, String>();
    public static final String EMPTY = "\u001B[42m";//"GREEN";
    public static final String ENERGY = "\u001B[43m";//"YELLOW";
    public static final String DANGER = "\u001B[41m";//"RED";
    
    public static final String Ranger = "BLUE";
    public static final String Wizard = "RED";
    public static final String Rogue = "PURPLE";
    
    public static final String RESET = "\u001B[0m";
    public static final String BLACK_TXT = "\u001B[30m";
    public static final String RED_TXT = "\u001B[31m";
    public static final String GREEN_TXT = "\u001B[32m";
    public static final String YELLOW_TXT = "\u001B[33m";
    public static final String BLUE_TXT = "\u001B[34m";
    public static final String PURPLE_TXT = "\u001B[35m";
    public static final String CYAN_TXT = "\u001B[36m";
    public static final String WHITE_TXT = "\u001B[37m";

    public static final String BLACK = "\u001B[40m";
    public static final String RED =   "\u001B[41m";
    public static final String GREEN = "\u001B[42m";
    public static final String YELLOW = "\u001B[43m";
    public static final String BLUE = "\u001B[44m";
    public static final String PURPLE = "\u001B[45m";
    public static final String CYAN = "\u001B[46m";
    public static final String WHITE = "\u001B[47m";
    
    /*public Colors() {
        list.put("RESET", "\u001B[0m");
        list.put("RED", "\u001B[41m");
        list.put("GREEN", "\u001B[42m");
        list.put("BLUE", "\u001B[44m");
        list.put("CYAN", "\u001B[46m");
        list.put("YELLOW", "\u001B[43m");
        list.put("PURPLE", "\u001B[45m");
        list.put("WHITE", "\u001B[47m");
    }
    public static String get(String culoare){
        
        switch(culoare.toUpperCase())
        {
            case "RESET": return "\u001B[0m";
            case "BLACK": return "\u001B[40m";
            case "RED": return "\u001B[41m";
            case "GREEN": return "\u001B[42m";
            case "BLUE": return "\u001B[44m";
            case "CYAN": return "\u001B[46m";
            case "YELLOW": return "\u001B[43m";
            case "PURPLE": return "\u001B[45m";
            case "WHITE": return "\u001B[47m";
            default: return "\u001B[47m";
        } 
    }
    
     public static String doBgColor(int cod){//cod=0...7
       return "\u001B["+(40+cod%8)+"m";
    }
     public static String doTxtColor(int cod){//cod=0...7
       return "\u001B["+(30+cod%8)+"m";
    }*/
    
}


package robots;

import java.util.ArrayList;

/*abstract class Observer extends Thread{
    protected ArrayList<Robot> subjects;
    
    public abstract void update(Point p);

    public abstract ArrayList<Robot> getSubjects();
}*/

abstract class MyObserver {
    protected Board subject;

    public abstract void update(Point p);
}

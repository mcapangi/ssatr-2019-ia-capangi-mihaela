package robots;

import java.util.ListIterator;
import java.util.Stack;

public class Path {
    private Stack<Point> points;

    public Path() {
        points=new Stack<>();
    }
   
    public void push(Point p)
    {
        points.push(p);
    }
    
    public Boolean isEmpty()
    {
        return points.isEmpty();
    }
    
    public Point pop()
    {
        Point p=points.peek();
        points.pop();
        return p;
    }
    
    public Point peek(){
        return points.peek();
    }
    
    public int size()
    {
        return points.size();
    }
    
    public int search(Point p){
        return points.search(p);
    }
    
    public Boolean contains(Point p)
    {
        //return points.search(p)>=0;
        ListIterator<Point> it = points.listIterator(points.size());
        while (it.hasPrevious()) {
            Point crt=it.previous();
            if(p.equals(crt ))
                    return true;
        }
        return false;
    }
    
    public Point leadsTo()
    {
        return points.firstElement();
    }
    
    public void write()
    {
        Point p;
        ListIterator<Point> it = points.listIterator(points.size());
        while (it.hasPrevious()) {
            p = it.previous();
            System.out.print(p+" ; ");
        }
        System.out.println("");
    }
    
}

package robots;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;


public class TestRobots {

   public static void testFight() {
    //public static void main(String[] args) {
        Board board = new Board(10);

        Robot a = new Robot("A", Colors.CYAN);
        a.setEnergy(5);
        a.setBoard(board);
        a.setLocation(board.pointAt(0, 0));
        a.findPath(new Point(0,9));


        Robot b = new Robot("B", Colors.BLUE);
        b.setEnergy(5);
        b.setBoard(board);
        b.setLocation(board.pointAt(0, 9));
        b.findPath(new Point(0,0));
        
        a.start();
        b.start();
        
        for (int i = 0; true ; i++) {
            System.out.println("Turn: " + i);
            board.write();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }

    }
   
   public static void testDanger() {
    //public static void main(String[] args) {
        Point p = new Point(0, 0);
        Board board = new Board(10);
        
        
        
        Robot a = new Robot("A", Colors.CYAN);
        a.setEnergy(1);
        a.setBoard(board);
        a.setLocation(board.pointAt(0, 0));
        a.findPath(new Point(9,9));
        a.getPath().write();
                 
        DangerObserver danO = new DangerObserver();
        board.addObserver(danO);
        
        a.start();
        
        for (int i = 0; true; i++) {
           board.generateDanger();
           System.out.println("Turn: " + i);
           board.write(); 
           try {
               Thread.sleep(1000);
           } catch (InterruptedException e) {
           }
       }

    }


    public static void main(String[] args) {
       
        Point p = new Point(0, 0);
        Board board = new Board(10);
        
        for (int i = 0; i < 4; i++) {
            board.pointAt(i, 4 - i).setEnergy(-1);
            board.pointAt(i + 5, 9 - i).setEnergy(-1);
        }
        
        Robot a = new Robot("A", Colors.CYAN);
        a.setEnergy(1);
        a.setBoard(board);
        a.setLocation(board.pointAt(0, 3));

        Robot b = new Robot("B", Colors.BLUE);
        b.setEnergy(1);
        b.setBoard(board);
        b.setLocation(board.pointAt(6, 9));
               
        EnergyObserver enO = new EnergyObserver();
        DangerObserver danO = new DangerObserver();
        board.addObserver(enO);
        board.addObserver(danO);
        a.start();
        b.start();
        
        int sec = 0,countDanger=8;
        Random r = new Random();
        for (int i = 0; true ; i++) {
            System.out.println("Turn: " + i);
            board.write();
            if (sec == 0) {
                if( r.nextInt(2)==0)        //countDanger<2*board.getSize() &&
                {
                    board.generateDanger();
                    countDanger++;
                }
                else
                    board.generateEnergy(); 
                sec = r.nextInt(board.getSize()) + 1;
                System.out.println("Generate next point in " + sec + " seconds.");
            } else {
                sec--;
            }
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
        
    }
    
}

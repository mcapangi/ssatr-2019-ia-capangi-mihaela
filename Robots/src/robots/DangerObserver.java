/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robots;

import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Profesor
 */
public class DangerObserver implements Observer{
   
    
    public void update(Observable o, Object arg){//Point p) {
        
        int d;
        Board b = (Board) o;
        Point p = (Point) arg;
        if(!b.pointAt(p).isDanger())
            return;
        for (int i=0;i<b.getSize();i++)
        {
            for(int j=0; j<b.getSize();j++)
            {
                Robot r=b.pointAt(i,j).getOwner();
                if(r==null || !r.hasPath() || !r.getPath().contains(p))
                    continue;
                Point dest=r.getPath().leadsTo();
                r.findPath(dest);
                if(r.getPath()!=null){
                   System.out.println("DangerObserver finds a new path for "+r.getName() + " to (" + dest.toString() + "), length = " + r.distance(dest)+" due to danger in path at "+p.toString());
                r.getPath().write(); 
                }
                else
                    System.out.println("DangerObserver stopped "+r.getName()+" due to danger in path at "+p.toString());
            }
        }
        
    }
}



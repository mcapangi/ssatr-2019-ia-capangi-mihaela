package robots;

import java.util.Observable;
import java.util.Observer;

public class EnergyObserver implements Observer {
    
    public void update(Observable o, Object arg){//Point p) {
        int dmin=Integer.MAX_VALUE,d;
        Robot rmin=null;
        Board b = (Board) o;
        Point p = (Point) arg;
        if(!b.pointAt(p).isEnergy())
            return;
        for (int i=0;i<b.getSize();i++)
        {
            for(int j=0; j<b.getSize();j++)
            {
                Robot r=b.pointAt(i,j).getOwner();
                if(r==null)continue;
                d=r.distance(p);
                if(d<=0 || r.hasPath() && r.getPath().size()<=d)
                    continue;
                if(d<dmin)
                {
                    dmin=d;
                    rmin=r;
                }
            }
        }
        
        if(rmin==null)
            return;
        rmin.findPath(p);
        System.out.println("EnergyObserver finds a path for "+rmin.getName() + " from ("+rmin.getLocation()+") to (" + p.toString() + "), length = " + rmin.distance(p));
        rmin.getPath().write();

    }
    

    
    
    /*public void run() {
        int i=0;
        while(true && i <10)
        {
            if(i%2==0)
                System.out.println(""+i);
            i++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
            
        }
    }*/

    

}

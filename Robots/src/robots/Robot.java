package robots;

import java.util.Random;


public class Robot extends Thread {

    private Point location;
    private int energy;
    private String name, color;
    private Board board;
    //private Stack<Point> path=new Stack<>();
    private Path path;

   
    
    public Robot() {
    }

    public Robot(String name, String color) {
        this.color = color;
        this.name = name;
        this.location = new Point(0, 0);
        this.energy = 0;
        this.setName(name);
    }

    public Point getLocation() {
        if (this.board != null) {
            return board.pointAt(location);
        } else {
            return location;
        }
    }

    public void setLocation(Point location) {
        if (this.board != null) {
            this.location = board.pointAt(location);
            board.pointAt(location).setOwner(this);
        } else {
            this.location = location;
        }
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getX() {
        return location.getX();
    }

    public void setX(int x) {
        this.location.setX(x);
    }

    public int getY() {
        return location.getY();
    }

    public void setY(int y) {
        this.location.setY(y);
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public void write() {
        if (color.equals(Colors.PURPLE) || color.equals(Colors.BLUE)) {
            System.out.print(Colors.WHITE_TXT + color + " " + name + " " + Colors.RESET + " ");
        } else {
            System.out.print(color + " " + name + " " + Colors.RESET + " ");
        }
    }

    public void drainEnergy() {
        if (location.getEnergy() > 0) {
            System.out.println(name + " drain energy: " + this.location.getEnergy());
            this.energy += this.location.getEnergy();
            this.location.setColor(Colors.EMPTY);
            location.setEnergy(0);
        }
    }

    public void move(String dir)//miscare cu un pas pe o directie precizata
    {
        this.location = this.location.nextPoint(dir);
    }

    public Boolean hasPath() {
        return path != null && !path.isEmpty();
    }

    public Boolean hasPathTo(Point dest) {
        return path != null && path.search(dest) != -1;
    }

    public Boolean findPath(Point dest) {
        path = board.findPath(location, dest);
        return path != null;
    }

    public int distance(Point dest)//gaseste o distanta pentru a atinge un obiectiv in timp minim, alg Lee
    {
        return board.distance(location, dest);
    }

    public void hit(Robot r) {
        if (this.energy == 0) {
            return;
        }
        Random nr = new Random();
        int e = nr.nextInt(this.energy+1);
        this.energy += Math.min(e, r.energy);
        r.energy -= Math.min(e, r.energy);

        System.out.println(name+" hits. Energy/hit " + e);
        System.out.println(String.format("Energy after hit: %s=%d %s=%d", name, energy, r.name, r.energy));
    }

    public void hit(Robot r, int e) {
        this.energy += e;
        r.energy -= e;
        System.out.println("Energy/hit " + e);
        System.out.println(String.format("Energy after hit: %s=%d %s=%d", name, energy, r.name, r.energy));
    }

    public void fight(Robot r) {
        System.out.println(String.format("Start fight %s vs. %s", name, r.name));
        int turn = 0;
        while (this.energy > 0 && r.energy > 0 && turn < 2) {
            turn++;
            this.hit(r);
            r.hit(this);
        }
        System.out.println(String.format("Stop fight %s vs. %s", name, r.name));
        System.out.println(String.format("Energy: %s=%d %s=%d", name, energy, r.name, r.energy));
    }

    public void fightToDeath(Robot r) {
        System.out.println(String.format("Start fight to death %s vs. %s", name, r.name));
        this.fight(r);
        if (this.energy > 0 && r.energy > 0) {
            if (this.energy >= r.energy) {
                this.hit(r, r.energy);
            } else {
                r.hit(this, this.energy);
            }
        }
        System.out.println(String.format("Stop fight to death %s vs. %s", name, r.name));
        System.out.println(String.format("Energy: %s=%d %s=%d", name, energy, r.name, r.energy));
        if(this.energy==0)
            die();
        if(r.energy==0)
            r.die();
    }

    public Point nextRandPoint() {
        Point p;
        do {
            p = location.nextPoint("NESV".charAt(new Random().nextInt(4)) + "");
        } while (!board.contains(p) || board.pointAt(p).isDanger());//||board.pointAt(p).owner!=null);
        return board.pointAt(p);
    }

    public void wander()//se muta un pas pe o directie generata aleator
    {
        if (energy == 0) {
            return;
        }
        Point p = nextRandPoint();
        if (p.getOwner() != null) {
            /*if (observers.isEmpty()) {
                this.fight(p.getOwner());
            }*/
            return;
        }
        synchronized (p) {
            move(p);
            drainEnergy();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }

    }

    public void goPath()//se muta un pas pe o cale minima
    {
       /* if (energy == 0) {
            die();
            return;
        }*/

        Point p = path.peek();
        Robot r = board.pointAt(p).getOwner();
        if (r != null && r.hasPath() && r.path.peek().equals(this.getLocation()) && this.path.peek().equals(r.getLocation())) {
            fightToDeath(r);
            //if (energy==0)
            //    return;
        }
        path.pop();
        synchronized (p) {
            move(p);
            drainEnergy();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }

    private void move(Point p) {
        board.pointAt(this.location).free();
        this.setLocation(p);
        // energy--;
    }

    private void die() {
        getLocation().free();
        //getLocation().setEnergy(-1);
        this.interrupt();
        System.out.println(getName() + " died!");
    }

    public void run() {
        int i = 0;
        while (energy > 0) {
                if (hasPath()) {
                    goPath();
                } else {
                    wander();
                }
      }
        //die();
    }
}


package lab1.g2.ex1;


public class PanouComandaExterior { //cate unul pe fiecare etaj
    
    private Controler ctrl;
    private int etajCurent;


    public PanouComandaExterior(Controler c,int etajCurent) {
        this.etajCurent = etajCurent;
        this.ctrl=c;
    }
        
    void cheamaLift()
    {
       ctrl.cheamaLift(etajCurent);
    }
}

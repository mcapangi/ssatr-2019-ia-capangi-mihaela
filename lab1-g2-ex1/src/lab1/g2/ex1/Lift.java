package lab1.g2.ex1;

public class Lift {
    private int etaj;
    static int maxim=10;

    public int getEtaj() {
        return etaj;
    }

    public void setEtaj(int etaj) {
        this.etaj = etaj;
    }
    
        
    Lift (int etaj)
    {
        this.etaj=etaj;
    }
    
    void urca()//acces package
    {
        if(etaj<maxim)
        {
            etaj++;
            System.out.println("Urca la: "+etaj);
        }
        else
        {
            System.out.println("Liftul este deja la ultimul etaj!");
        }
    }
    
    void coboara()//acces package
    {
        if(etaj>0)
        {
            etaj--;
            System.out.println("Coboara la: "+etaj);
        }
        else
        {
            System.out.println("Liftul este deja la parter!");
        }
    }
}

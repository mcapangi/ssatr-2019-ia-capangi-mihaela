/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wateringsystem;

/**
 *
 * @author Profesor
 */
public class Valve {

    private Boolean stare;
    private String nume;

    public Valve(Boolean stare, String nume) {
        this.stare = stare;
        this.nume = nume;
    }

    public Valve(String nume) {
        stare = false;
        this.nume = nume;
    }

    public Boolean getStare() {
        return stare;
    }

    public void setStare(Boolean stare) {
        this.stare = stare;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    void open() {
        stare = true;
        System.out.println("Am deschis " + nume);
    }

    void close() {
        stare = false;
        System.out.println("Am oprit " + nume);
    }

    public String toString() {
        return "Valva: " + nume + "; stare: " + stare;
    }
}

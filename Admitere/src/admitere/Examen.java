//https://www.javamex.com/tutorials/collections/using_4.shtml

package admitere;


import java.util.*;
import java.io.*;

public class Examen
{
    public List<Candidat> candidati=new ArrayList<Candidat>();
    public Map<Integer,Sectie> sectii = new HashMap<Integer,Sectie>(); 
    
    
    public Examen()
    {
    //candidati.add(new Candidat(0,"ana",10,8,0));
    //candidati.add(new Candidat(1,"paul",10,8,0));
       candidati=new ArrayList<Candidat>();
       sectii = new HashMap<Integer,Sectie>();
    }
    
    public Examen(String fisCandidati, String fisSectii)  
    {
       CandidatiDinFisier(fisCandidati);
       SectiiDinFisier(fisSectii);
    }
    
    public int nrCandidati()
    {
        return candidati.size(); 
    }
    
    public boolean CandidatiDinFisier(String fisier) 
    {

        candidati=new ArrayList<Candidat>();
        if(fisier=="")
            fisier="candidati.txt";
        try
        {
            File file = new File(fisier);//("candidati.txt"); 
            BufferedReader br = new BufferedReader(new FileReader(file)); 
            String linie; 
      
            while ((linie = br.readLine()) != null) 
            {
                if(linie.trim().compareTo("")==0)
                    continue;
                candidati.add(new Candidat(linie));
            }
            br.close();
        }
        catch(FileNotFoundException ex)
        {
            System.err.println("FileNotFoundException: " + ex.getMessage());
            return false;
        }
        catch (IOException ex)
        {
            System.err.println("IOException: " + ex.getMessage());
            return false;
        }
    
    return true;
    }
    
    public boolean SectiiDinFisier(String fisier)  
    {
       sectii = new HashMap<Integer,Sectie>();
       if(fisier=="")
            fisier="sectii.txt";
       try
       {
            File file = new File(fisier);
            BufferedReader br = new BufferedReader(new FileReader(file)); 
            String linie; 
            while ((linie = br.readLine()) != null) 
            {
                if(linie.trim().compareTo("")==0)
                    continue;
                Sectie s=new Sectie(linie);
                sectii.put(s.id,s);
            }
            br.close();
        }
        catch(FileNotFoundException ex)
        {
            System.err.println("FileNotFoundException: " + ex.getMessage());   
            return false;
        }
        catch (IOException ex)
        {
            System.err.println("IOException: " + ex.getMessage());
            return false;
        }
        return true;
    }
    
    public void Admitere()
    {
        Map<Integer,Sectie> tmp=new HashMap<Integer,Sectie>(); 
        //tmp.putAll(sectii);
        for(Integer i: sectii.keySet())
        {
            Sectie s=new Sectie(sectii.get(i));// (sectii.get(i).id, sectii.get(i).nume, sectii.get(i).locuri);
            tmp.put(s.id,s);
        }
        Collections.sort(candidati,Candidat.comparaMedie);
        for (Candidat c : candidati) 
        {
                if(c.stare==Candidat.INCORECT)continue;
                c.stare=Candidat.RESPINS;
                if(c.acceptat())
                {
                    for(Integer o : c.optiuni)
                    {
                        if(tmp.get(o)!=null && tmp.get(o).locuri>0)
                        {
                            c.stare=o;
                            tmp.get(o).locuri--;
                            break;
                        }
                    }
                }
   
        }
    }
    
     public int NrLocuriOcupate(int id)
    {
        int nr=0;
        for (Candidat c : candidati) 
        {
            if(c.stare==id)
            {
                nr++;
            }
        }
        return nr;
    }
    public void AfisareLista(List<Candidat> l, int tipScriere, String titlu)
    {
        System.out.println(titlu);
        for (Candidat c : l) 
        {
            switch(tipScriere)
            {
                case 1: System.out.println(c.toString());break;
                case 2: System.out.println(c.toStringShort());break;
                case 3: System.out.println(c.toStringVeryShort());break;
            }
        }
        System.out.println("Total: "+l.size()+" candidati");
        System.out.println();
    }
    
    public List<Candidat> ListaSectie(int id)
    {
        List<Candidat> l=new ArrayList<Candidat>();
        for (Candidat c : candidati) 
        {
            if(c.stare==id)
            {
                l.add(c);
            }
        }
        return l;
    }
    
    public void ListaAdmisi(int tipScriere)
    {
        List<Candidat> l=new ArrayList<Candidat>();
        for(Integer i: sectii.keySet())
        {
            Sectie s=sectii.get(i);
            String titlu=s.nume+" (" + s.locuri+" locuri)";
            l=ListaSectie(sectii.get(i).id);
            AfisareLista(l,tipScriere,titlu);
        }

    }
    
    public List<Candidat> ListaNonAdmisi(int stare) //RESPINS.INCORECT.NEPRELUCRAT
    {
        List<Candidat> l=new ArrayList<Candidat>();
        for (Candidat c : candidati) 
        {
            if(c.stare==stare)
            {
                l.add(c);
            }
        }
        Collections.sort(l, new Comparator<Candidat>(){
            public int compare(Candidat x, Candidat y){
               return x.nume.compareTo(y.nume);
               }
        });
        return l;
    }
    Candidat ultimulAdmis (int idS)
    {
        Candidat u=new Candidat();
        for(Candidat c : candidati)
            if(c.stare==idS)
                u=c;
        return u;
    }    
    public void ListaSpeciala(int tipScriere)
    {
        System.out.println("Candidatii cu criterii egale cu ultimul admis: ");
        int total=0;
        List<Candidat> R=ListaNonAdmisi(Candidat.RESPINS);

        for(Integer i: sectii.keySet())
        {
            List<Candidat> tmp=new ArrayList<Candidat>();
            Sectie s=sectii.get(i);
            Candidat u = ultimulAdmis(s.id);
            for (Candidat c : R) 
            {
                if(c.optiuni.contains(s.id) && c.criteriiEgale(u))
                    tmp.add(c);
            }
            if(tmp.size()!=0)
            {
                String titlu=s.nume+" (" + tmp.size()+" candidati)";
                AfisareLista(tmp,tipScriere,titlu);
                total+=tmp.size();
            }
        }
        //System.out.println("Total: "+total+" candidati");
        
        
        /*for (Candidat c : candidati) 
        {
            if(c.stare==Candidat.RESPINS)
                {
                    for(Integer o : c.optiuni)
                    {
                        if(sectii.get(o)!=null && c.criteriiEgale(ultimul(o)) )
                        {
                            l.add(c);
                            break;
                        }
                    }
                }
        }
        Collections.sort(l, new Comparator<Candidat>(){
            public int compare(Candidat x, Candidat y){
               return x.nume.compareTo(y.nume);
               }
        });
        System.out.println("Candidatii cu criterii egale cu ultimul admis: " + l.size());*/
        //AfisareLista(l, tipScriere);
    }
    
    public void Liste()
    {
        List<Candidat> l=new ArrayList<Candidat>();
        ListaAdmisi(2);
        
        l=ListaNonAdmisi(Candidat.RESPINS); 
        AfisareLista(l, 2,"Candidati respinsi: ");
        
        ListaSpeciala(1);
        
        l=ListaNonAdmisi(Candidat.INCORECT);
        AfisareLista(l, 1,"Candidati cu date incorecte: ");
        
        l=ListaNonAdmisi(Candidat.NEPRELUCRAT); 
        AfisareLista(l, 1,"Candidati cu date neprelucrate: ");
    }
        
    
    /*public static void main()
    {
        Examen E=new Examen("","");
        E.Admitere();
        E.Liste();
    }*/
}

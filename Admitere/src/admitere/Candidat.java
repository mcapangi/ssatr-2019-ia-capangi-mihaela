/*
https://howtodoinjava.com/sort/collections-sort/

 */
package admitere;

import java.util.*;

public class Candidat {

    public static final int NEPRELUCRAT = 0, RESPINS = -1, INCORECT = -2;
    public int id;
    public String nume;
    public double proba1, proba2, baraj;
    public int stare;//0=neprelucrat,-1=respins,>0=id sectie admis
    public List<Integer> optiuni = new ArrayList<Integer>();

    public static Comparator<Candidat> comparaNume = (Candidat c1, Candidat c2) -> c1.nume.compareTo(c2.nume);
    public static Comparator<Candidat> comparaMedie = (Candidat c1, Candidat c2) -> c1.medie().compareTo(c2.medie());

    public Candidat() {
        proba1 = proba2 = baraj = 0;
        id = 0;
        nume = "";
        stare = NEPRELUCRAT;
    }

    public Candidat(int id, String nume) {
        this.id = id;
        this.nume = nume;
    }

    public Candidat(int id, String nume, double proba1, double proba2, double baraj) {
        this.id = id;
        this.nume = nume;
        this.proba1 = proba1;
        this.proba2 = proba2;
        this.baraj = baraj;
        stare = NEPRELUCRAT;
    }

    public Candidat(String linie) {
        Numar n = new Numar();
        String[] s = linie.split(";");
        if (s.length < 6) {
            n.corect = false;
            s = Arrays.copyOf(s, 6);
        }
        id = n.tryParseInt(s[0]);
        nume = s[1];
        proba1 = n.tryParseDouble(s[2]);
        proba2 = n.tryParseDouble(s[3]);
        baraj = n.tryParseDouble(s[4]);
        for (int i = 5; i < s.length; i++) {
            optiuni.add(n.tryParseInt(s[i]));
        }
        if (n.corect) {
            stare = NEPRELUCRAT;
        } else {
            stare = INCORECT;
        }
    }
    
    public Double medie() {  //calculeaza media ponderata cu doua zecimale
        if (stare == INCORECT) {
            return 0.0;
        }
        Double m = proba1 * Criterii.pondereProba1 + proba2 * Criterii.pondereProba2;
        m = Math.floor(m * 100) / 100;
        return m;
    }

    public int compareTo(Candidat c) {
        if (this.medie() > c.medie()) {
            return -1;
        }
        if (this.medie() < c.medie()) {
            return 1;
        }
        if (this.baraj > c.baraj) {
            return -1;
        }
        if (this.baraj < c.baraj) {
            return 1;
        }
        if (this.proba2 > c.proba2) {
            return -1;
        }
        if (this.proba2 < c.proba2) {
            return 1;
        }
        return this.nume.compareTo(c.nume);
        //return this.medie()>c.medie()?1:this.medie()<c.medie()?-1:this.baraj>c.baraj?1:this.baraj<c.baraj?-1:this.nume.compareTo(c.nume);
    }

    public Boolean criteriiEgale(Candidat c) {
        return (medie() == c.medie() && baraj == c.baraj && proba2 == c.proba2);
    }

    public String toString() {
        String r = "";
        r += id + ";" + nume + ";" + medie() + " (" + proba1 + ";" + proba2 + ";" + baraj + "; Optiuni:";//+stare+";";
        for (Integer i : optiuni) {
            r += i + ";";
        }
        r += ")";
        return r;
    }

    public String toStringShort() {
        String r = "";
        r += id + "; " + nume + "; " + medie();//+"; "+stare;
        return r;
    }

    public String toStringVeryShort() {
        String r = "";
        r += id + "; " + nume + "; ";//+"; "+stare;
        return r;
    }

    public boolean acceptat() {
        return (proba1 >= Criterii.notaminima && proba2 >= Criterii.notaminima && medie() >= Criterii.medieMinima );
    }

    public boolean valid() {
        if (proba1 == 0 || proba2 == 0 || baraj == 0) {
            return false;
        }
        if (optiuni.isEmpty()) {
            return false;
        } 
        return true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public double getProba1() {
        return proba1;
    }

    public void setProba1(double proba1) {
        this.proba1 = proba1;
    }

    public double getProba2() {
        return proba2;
    }

    public void setProba2(double proba2) {
        this.proba2 = proba2;
    }

    public double getBaraj() {
        return baraj;
    }

    public void setBaraj(double baraj) {
        this.baraj = baraj;
    }

    public int getStare() {
        return stare;
    }

    public void setStare(int stare) {
        this.stare = stare;
    }

    public List<Integer> getOptiuni() {
        return optiuni;
    }

    public void setOptiuni(List<Integer> optiuni) {
        this.optiuni = optiuni;
    }
    
    

}

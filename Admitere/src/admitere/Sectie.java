
package admitere;

import java.util.*;

public class Sectie
{
    public int id;
    public String nume;
    public int locuri;

    public Sectie()
    {
        id=0;
        nume="";
        locuri=0;
    }
    
    public Sectie(int id, String nume, int locuri)
    {
        this.id=id;
        this.nume=nume;
        this.locuri=locuri;
    }
    
    public Sectie(Sectie s)
    {
        this.id=s.id;
        this.nume=s.nume;
        this.locuri=s.locuri;
    }
    public Sectie (String linie)
    {
        Numar n= new Numar();
        String[] s = linie.split(";"); 
        if(s.length<3)
            s = Arrays.copyOf(s, 3);
        id=n.tryParseInt(s[0]);
        nume=s[1];
        locuri=n.tryParseInt(s[2]);
    }
    
    public String toString()
    {
        String r="";
        r+=id+";"+nume+";"+locuri+";";
        return r;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public int getLocuri() {
        return locuri;
    }

    public void setLocuri(int locuri) {
        this.locuri = locuri;
    }
    
    

}

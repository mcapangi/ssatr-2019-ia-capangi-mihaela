 
package admitere;

public class Numar  
{
    Boolean corect;
    Numar ()
    {
        corect=true;
    }
    public Integer tryParseInt(String text)
    {
      try 
      {
          return Integer.parseInt(text);
      } 
      catch (NumberFormatException e) 
      {
          corect=false;
          return 0;
      }
    }
    
    public Double tryParseDouble(String text) 
    {
      if(text==null)
           {text="0.0";corect=false;}
      try 
      {
          return Double.parseDouble(text);
      } 
      catch (NumberFormatException e) 
      {
          corect=false;
          return 0.0;
      }
    }
    
    public void test()
    {
        Boolean ok=true;
        System.out.println(tryParseDouble("*"));
        System.out.println(corect);
        System.out.println(tryParseDouble("0.7"));
        System.out.println(corect);
        /*System.out.println(tryParseDouble("*2"));
        System.out.println(tryParseDouble("2*"));
        System.out.println(tryParseDouble("2.3"));
        System.out.println(tryParseInt(""));
        System.out.println(tryParseInt(null));*/
    }
}


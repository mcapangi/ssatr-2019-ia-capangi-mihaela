
package admitere;

public class Criterii {
    public static double medieMinima=6, notaminima=5;
    public static double pondereProba1=0.6, pondereProba2=0.4;

    public static double getMedieMinima() {
        return medieMinima;
    }

    public static void setMedieMinima(double medieMinima) {
        Criterii.medieMinima = medieMinima;
    }

    public static double getNotaminima() {
        return notaminima;
    }

    public static void setNotaminima(double notaminima) {
        Criterii.notaminima = notaminima;
    }

    public static double getPondereProba1() {
        return pondereProba1;
    }

    public static void setPondereProba1(double pondereProba1) {
        Criterii.pondereProba1 = pondereProba1;
    }

    public static double getPondereProba2() {
        return pondereProba2;
    }

    public static void setPondereProba2(double pondereProba2) {
        Criterii.pondereProba2 = pondereProba2;
    }
    
}
